from django.contrib import admin
from patient.models import Patient, Gender, CountryCode, PhoneType, EmailType, PatientPhone, PatientEmail,\
    Phone, Email
from django import forms


class PatientPhoneT(admin.TabularInline):
    model = PatientPhone
    extra = 1


class PatientEmailT(admin.TabularInline):
    model = PatientEmail
    extra = 1


class PatientPhoneAdmin(admin.ModelAdmin):
    list_display = ('patient', 'priority', 'phones')


class PatientEmailAdmin(admin.ModelAdmin):
    list_display = ('patient', 'emails')


class PhoneAdmin(admin.ModelAdmin):
    inlines = [PatientPhoneT, ]


class PatientAdmin(admin.ModelAdmin):
    list_display = ('identifier', 'full_name', 'gender', 'get_phones', 'get_emails')

    def get_phones(self, obj):
        """Funtion to get the phones related with the patient"""
        phone_list = []
        for p in obj.patientphone_set.all().order_by('priority'):
            phone_number = str(p.phones.phone_number)
            priority = str(p.priority)
            phone_list.append(priority+': '+phone_number)
            phone = ",  ".join(phone_list)
        return phone

    def get_emails(self, obj):
        """Funtion to get the emails related with the patient"""
        email_list = []
        for p in obj.patientemail_set.all().order_by('priority'):
            email = str(p.emails.email)
            priority = str(p.priority)
            email_list.append(priority + ': ' + email)
            email = ",  ".join(email_list)
        return email


class CountryCodeAdmin(admin.ModelAdmin):
    list_display = ('code', )


class GenderAdmin(admin.ModelAdmin):
    list_display = ('gender', )


class PhoneTypeAdmin(admin.ModelAdmin):
    list_display = ('description', )


class EmailTypeAdmin(admin.ModelAdmin):
    list_display = ('description', )


class EmailAdmin(admin.ModelAdmin):
    inlines = [PatientEmailT, ]


admin.site.register(Gender, GenderAdmin)
admin.site.register(CountryCode, CountryCodeAdmin)
admin.site.register(PhoneType, PhoneTypeAdmin)
admin.site.register(EmailType, EmailTypeAdmin)
admin.site.register(Phone, PhoneAdmin)
admin.site.register(Email, EmailAdmin)
admin.site.register(Patient, PatientAdmin)
admin.site.register(PatientPhone, PatientPhoneAdmin)
admin.site.register(PatientEmail, PatientEmailAdmin)




