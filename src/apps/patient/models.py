import re
from django.db import models
from django.core.exceptions import ValidationError


class Gender(models.Model):
    """Model for genders"""
    gender = models.CharField(max_length=10)

    def __str__(self):
        return self.gender

    class Meta:
        db_table = 'genders'


class CountryCode(models.Model):
    """Model for country codes"""
    code = models.IntegerField()

    def __unicode__(self):
        return self.code

    class Meta:
        db_table = 'country_codes'


class PhoneType(models.Model):
    """Model for phones types"""
    description = models.CharField(max_length=20)

    def __str__(self):
        return self.description

    class Meta:
        db_table = 'phone_types'


class EmailType(models.Model):
    """Model for email types"""
    description = models.CharField(max_length=20)

    def __str__(self):
        return self.description

    class Meta:
        db_table = 'email_types'


def phone_length(phone):
    """function to validate that the phone number is min 8 and max 10 characters"""
    phone_len = len(str(phone))
    if phone_len not in range(8, 11):
        raise ValidationError('Number should have between 8 and 10 characters')


class Phone(models.Model):
    """Model for patients phones numbers information"""
    phone_type = models.ForeignKey(PhoneType, on_delete=models.DO_NOTHING)
    country_code = models.ForeignKey(CountryCode, on_delete=models.DO_NOTHING)
    phone_number = models.BigIntegerField(validators=[phone_length], unique=True) #In the histories the requirement is
    #PositiveIntegerField but if I use this number 6643367420 I get and error, I search in google and it has a problem
    #with the limit of the Integer.
    phone_notes = models.TextField(null=True, blank=True)

    def __str__(self):
        """Method to give phone number a format: +country_code(lada)###+#### """
        splitat = 3
        lada, phone_number = str(self.phone_number)[:splitat], str(self.phone_number)[splitat:]
        secondsplit = 3
        phone_number_1, phone_number_2 = str(phone_number)[:secondsplit], str(phone_number)[secondsplit:]
        phone_number = '+'+str(self.country_code.code)+'('+lada+')'+phone_number_1+'-'+phone_number_2
        return phone_number

    class Meta:
        db_table = 'phones'


class Email(models.Model):
    """Model for patients emails information"""
    email_type = models.ForeignKey(EmailType, on_delete=models.DO_NOTHING)
    email = models.EmailField(max_length=50, unique=True)
    email_notes = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'emails'

    def __str__(self):
        return self.email


class Patient(models.Model):
    """Model for patients information"""
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    gender = models.ForeignKey(Gender, on_delete=models.DO_NOTHING)
    is_active = models.BooleanField(default=True)
    identifier = models.CharField(max_length=10, unique=True, editable=False)
    patient_notes = models.TextField(null=True, blank=True)
    phones = models.ManyToManyField(Phone, through='PatientPhone')
    emails = models.ManyToManyField(Email, through='PatientEmail')

    def save(self, *args, **kwargs):
        """Overwrite save method to clean first name and last name form the extra blank spaces,
        this method clean all the spaces and the begin, middle or end of the chain on character and change it to
        uppercase"""
        first_name = self.first_name
        last_name = self.last_name
        self.first_name = " ".join(first_name.split()).upper()
        self.last_name = " ".join(last_name.split()).upper()
        super(Patient, self).save(*args, **kwargs)

        if self.pk:
            """this bloke of code creates de identifier number for each patient, it adds 0 to the left so the length of 
            the identifier is always 10"""
            num = str(self.pk)
            num = num.zfill(7)
            self.identifier = 'PAT' + num
            super(Patient, self).save(*args, **kwargs)

    def full_name(self):
        """Function to return the full name of the patient"""
        return '%s %s' % (self.first_name, self.last_name)

    def __str__(self):
        return self.full_name()

    class Meta:
        db_table = 'patients'


class PatientPhone(models.Model):
    """Model for relationship between patients_phones
     so the object of the many to many can be changed if needed"""
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    phones = models.ForeignKey(Phone, on_delete=models.DO_NOTHING)
    priority = models.IntegerField()

    def clean(self, *args, **kwargs):
        """This functions looks is the patient already have a phone number with the priority sent in the post and send
        and error if it does"""
        num_results = PatientPhone.objects.filter(patient=self.patient, priority=self.priority).count()
        if num_results >= 1:
            raise ValidationError({
                'priority': (
                    'This Patient all ready have a Phone number with this priority'
                ),
            })

    class Meta:
        db_table = 'phones_patient'
        ordering = ['priority']
        unique_together = ['patient', 'phones']


class PatientEmail(models.Model):
    """Model for relationship between patients_emails
         so the object of the many to many can be changed if needed """
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    emails = models.ForeignKey(Email, on_delete=models.DO_NOTHING)
    priority = models.IntegerField()

    class Meta:
        db_table = 'emails_patient'
        ordering = ['priority']
        unique_together = ['patient', 'emails']

    def clean(self, *args, **kwargs):
        """This functions looks is the patient already have an email with the priority sent in the post and send
                and error if it does"""
        num_results = PatientEmail.objects.filter(patient=self.patient, priority=self.priority).count()
        if num_results >= 1:
            raise ValidationError({
                'priority': (
                    'This Patient all ready have a Email with this priority'
                ),
            })
